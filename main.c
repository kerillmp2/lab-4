#include "header.h"
#include "linked.c"

struct linked_list* read_list_from_input( void ) {
	int value;
	struct linked_list* list = ( struct linked_list* ) malloc( sizeof( struct linked_list ) );

	list->size = 0;

	puts("Enter numbers...");
	while( scanf( "%d", &value ) != EOF ){
		if ( list->size == 0 ){
			list = list_create( value );
		}else{
			list_add_front( value, &list );
		}
	}

	return list;
}

void print_element( int index, struct linked_list** list ) {
	struct node* node_at_index = list_node_at( index, list );
	if( node_at_index == NULL ) {
		printf( "List does not contain that many elements" );
	} else {
		printf( "Value of element at index %d is %d\n", index, node_at_index->value );
	}
	return;
}


int main( void ) {
	struct linked_list* list = read_list_from_input();
	printf( "Sum: %d\n", list_sum( &list ) );
	print_element( 3, &list );
	list_free( &list );
	return 0;
}

